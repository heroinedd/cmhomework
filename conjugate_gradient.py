from typing import List
from numpy import linalg

import numpy as np
import matplotlib.pyplot as plt


def conjugate_gradient(A: np.ndarray, b: np.ndarray, x0: np.ndarray, epsilon: np.float64):
    """
    :param A:
    :param b:
    :param x0:
    :param epsilon:
    :return: x*, [r0, r1, r2, ..., rn]
    """
    x = x0
    r = b - A.dot(x)
    d = r
    error = [r]
    for i in range(b.shape[0]):
        alpha = r.T.dot(r) / d.T.dot(A).dot(d)
        x = x + alpha * d
        r_old = r
        r = b - A.dot(x)
        error.append(r)
        if linalg.norm(r) <= epsilon:
            break
        beta = linalg.norm(r) ** 2 / linalg.norm(r_old) ** 2
        d = r + beta * d
    return x, error


# (A(xk - tk * dk) - b) * (-(Axk - b)) = 0 -> tk = (Axk - b) / (A * dk)
def newton(A: np.ndarray, b: np.ndarray, x0: np.ndarray, epsilon: np.float64, alpha: np.float64):
    x = x0
    r = b - A.dot(x)
    error = [r]
    while linalg.norm(r) > epsilon:
        x = x + alpha * r
        r = b - A.dot(x)
        error.append(r)
    return x, error


def plot_converge_speed(error: List[np.ndarray], method: str):
    x = np.asarray([i for i in range(len(error))])
    y = np.asarray([linalg.norm(i) for i in error])

    ax = plt.figure().gca()
    # force only integer values for x-axis
    ax.xaxis.get_major_locator().set_params(integer=True)

    plt.plot(x, y)
    plt.yscale('log')
    plt.xlabel('round')
    plt.ylabel('error')
    plt.title(method)

    # plt.show()
    plt.savefig(method + '.jpg')


def example():
    eg = """
    | 2 0 1 |     | 3 |       | 0 |
    | 0 1 0 | x = | 1 |, x0 = | 0 |, epsilon = 1e-5
    | 1 0 2 |     | 3 |       | 0 |
    """
    print('共轭梯度法示例：')
    print(eg)
    A = np.asarray([[2, 0, 1], [0, 1, 0], [1, 0, 2]], dtype=np.float64)
    b = np.asarray([[3], [1], [3]], dtype=np.float64)
    x0 = np.asarray([[0], [0], [0]], dtype=np.float64)
    epsilon = np.float64(1e-5)

    # conjugate gradient
    print('共轭梯度法')
    ans0, error0 = conjugate_gradient(A, b, x0, epsilon)
    print(ans0)

    # newton
    print('牛顿法')
    alpha = np.float64(0.1)
    ans1, error1 = newton(A, b, x0, epsilon, alpha)
    print(ans1)

    # plot_converge_speed(error0, 'conjugate gradient')
    # plot_converge_speed(error1, 'newton')


if __name__ == "__main__":
    example()
