# 计算方法上机实验报告

姓名：王丹

学号：3121151027

## 利用共轭梯度法求解大规模稀疏方程组

### 方法简介

共轭梯度法是把求解线性方程组的问题转化为求解一个与之等价的二次函数极小化的问题，从任意给定的初始点出发，沿一组关于矩阵$A$的共轭方向进行线性搜索，在无舍入误差的假定下，最多迭代$n$次（$n$是矩阵$A$的阶数），就可求得二次函数的极小点，也就求得了线性方程组$Ax=b$的解。 <br>
注意：利用共轭梯度法求解线性方程组$Ax=b$的解要求系数矩阵$A$对称正定。

### 伪代码
1. 给定初始近似点$x^{(0)}$及精度$\epsilon>0$；

2. 计算$r^{(0)}=b-Ax^{(0)}$，取$d^{(0)}=r^{(0)}$；

3. $for~k=0,1,...,n-1~do$

    (1) $ \alpha_k = \frac{{r^{(k)T}}{r^{(k)}}}{{d^{(k)T}}A{d^{(k)}}} $ <br>
    (2) $ x^{(k+1)} = x^{(k)} + \alpha_kd^{(k)} $ <br>
    (3) $ r^{(k+1)} = b - Ax^{(k+1)} $ <br>
    (4) 若$ \parallel r^{(k+1)} \parallel \leq \epsilon $或$ k + 1 = n $，则输出近似解$ x^{(k+1)} $，停。否则转(5) <br>
    (5) $ \beta_k = -\frac{r^{(k+1)}Ad^{(k)}}{d^{(k)T}Ad^{(k)}} = \frac{\parallel r^{(k+1)} \parallel_2^2}{\parallel r^{(k)} \parallel_2^2} $ <br>
    (6) $ d^{(k+1)} = r^{(k+1)} + \beta_k d^{(k)} $ <br>
    $end~do$ <br>

### 代码

#### 共轭梯度法

```python
from numpy import linalg
import numpy as np


def conjugate_gradient(A: np.ndarray, b: np.ndarray, x0: np.ndarray, epsilon: np.float64):
    """
    :param A:
    :param b:
    :param x0: 初始值
    :param epsilon: 精度
    :return x*, [r0, r1, r2, ..., rn]: 近似解和每一步迭代的误差
    """
    x = x0
    r = b - A.dot(x)
    d = r
    error = [r]
    for i in range(b.shape[0]):
        alpha = r.T.dot(r) / d.T.dot(A).dot(d)  # step(1)
        x = x + alpha * d  # step(2)
        r_old = r
        r = b - A.dot(x)  # step(3)
        error.append(r)
        if linalg.norm(r) <= epsilon:  # step(4)
            break
        beta = linalg.norm(r) ** 2 / linalg.norm(r_old) ** 2  # step(5)
        d = r + beta * d  # step(6)
    return x, error
```

#### 牛顿下降法

```python
def newton(A: np.ndarray, b: np.ndarray, x0: np.ndarray, epsilon: np.float64, alpha: np.float64):
    """
    :param A:
    :param b:
    :param x0: 初始值
    :param epsilon: 精度
    :param alpha: 步长
    :return x*, [r0, r1, r2, ..., rn]: 近似解和每一步迭代的误差
    """
    x = x0
    r = b - A.dot(x)
    error = [r]
    while linalg.norm(r) > epsilon:
        x = x + alpha * r
        r = b - A.dot(x)
        error.append(r)
    return x, error
```

### 具体算例和结果

#### 算例

$$
\begin{pmatrix}
2 & 0 & 1 \\
0 & 1 & 0 \\
1 & 0 & 2 \\
\end{pmatrix}
x=
\begin{pmatrix}
3 \\
1 \\
3 \\
\end{pmatrix}
,
x_0=
\begin{pmatrix}
0 \\
0 \\
0 \\
\end{pmatrix}
$$

#### 结果

1. 共轭梯度法
   $$
   x^*=
   \begin{pmatrix}
   1 \\
   1 \\
   1 \\
   \end{pmatrix}
   $$
   收敛速度图：

   <img src="E:\计算方法\上机\cmhomework\figures\conjugate gradient.jpg" alt="conjugate gradient" style="zoom:80%;" />

2. 牛顿法
   $$
   x^*=
   \begin{pmatrix}
   1 \\
   0.99999074 \\
   1 \\
   \end{pmatrix}
   $$
   收敛速度图：

   <img src="E:\计算方法\上机\cmhomework\figures\newton.jpg" style="zoom:80%;" />

## 最小二乘拟合问题的求解

### 方法简介

#### 最小二乘拟合多项式

设$f(x)$是定义在点集$X={x_1, x_2, ..., x_m}$上的列表函数或者$f(x)$是定义在区间$[a,b]$上表达式复杂的连续函数。构造广义多项式
$$
p(x)=c_0\phi_0(x)+c_1\phi_1(x)+...+c_n\phi_n(x)
$$
使得
$$
\parallel p-f \parallel_2^2
$$
达到最小，其中$c_0,c_1,...,c_n$为待定参数，$\phi_i(x)~(i=0,1,...,n)$为已知的一组线性无关的函数（称为基函数，它的选取与具体问题有关）。取$p(x)$作为$f(x)$的近似表达式就是**最优平方逼近问题**。

当$f(x)$是定义在点集$X={x_1, x_2, ..., x_m}$上的列表函数时，设在点$x_i$处的函数值为$y_i$，则
$$
\parallel p-f \parallel_2^2 = (p-f, p-f)=\sum_{i=1}^m\omega_i(p(x_i)-y_i)^2
$$
这时，所求得的$p(x)$称为**最小二乘拟合函数**。若取$\phi_i(x)=x^i~(i=0,1,...,n)$，则
$$
p(x) = p_n(x) = c_0 + c_1x + c_2x^2 + ... + c_nx^n
$$
称为**最小二乘拟合多项式**。

#### 正规方程组

求解最小二乘拟合函数或最优平方逼近函数$p(x)$中的系数$c_0,c_1,...,c_n$的线性方程组为：
$$
\sum_{j=0}^n(\phi_k, \phi_j)c_j = (\phi_k,f),~k=0,1,...,n
$$
其矩阵形式为：
$$
\begin{pmatrix}
(\phi_0, \phi_0) & (\phi_0, \phi_1) & \cdots & (\phi_0, \phi_n) \\
(\phi_1, \phi_0) & (\phi_1, \phi_1) & \cdots & (\phi_1, \phi_n) \\
\vdots & \vdots & \ddots & \vdots\\
(\phi_n, \phi_0) & (\phi_n, \phi_1) & \cdots & (\phi_n, \phi_n) \\
\end{pmatrix}
\begin{pmatrix}
c_0 \\
c_1 \\
\vdots \\
c_n
\end{pmatrix}
=
\begin{pmatrix}
(\phi_0, f) \\
(\phi_1, f) \\
\vdots \\
(\phi_n, f) \\
\end{pmatrix}
$$
对于最小二乘拟合多项式，其$\phi_i(x)=x^i~(i=0,1,...,n)$，所以其正规方程组为：
$$
\begin{pmatrix}
\sum_{i=1}^m1 & \sum_{i=1}^mx_i & \cdots & \sum_{i=1}^mx_i^n \\
\sum_{i=1}^mx_i & \sum_{i=1}^mx_i^2 & \cdots & \sum_{i=1}^mx_i^{n+1} \\
\vdots & \vdots & \ddots & \vdots \\
\sum_{i=1}^mx_i^n & \sum_{i=1}^mx_i^{n+1} & \cdots & \sum_{i=1}^mx_i^{2n} \\
\end{pmatrix}
\begin{pmatrix}
c_0 \\
c_1 \\
\vdots \\
c_n \\
\end{pmatrix}
=
\begin{pmatrix}
\sum_{i=1}^my_i \\
\sum_{i=1}^mx_iy_i \\
\vdots \\
\sum_{i=1}^mx_i^ny_i \\
\end{pmatrix}
$$
由于正规方程组的系数矩阵是对称正定矩阵，所以：1）正规方程组的解存在且唯一，即最小二乘拟合函数或最优平方逼近函数$p(x)$存在且唯一；2）正规方程组可用第一部分的共轭梯度法求解。

### 程序流程图

```mermaid
graph LR
	A(输入x,y和多项式阶数r) --> B(构造系数矩阵A)
	B --> C(构造b)
	C --> D(使用共轭梯度法计算Ax=b)
```

### 代码

```python
def least_square_fitting(x: [], y: [], r: int, epsilon=np.float64(1e-5)):
    """
    :param x:
    :param y:
    :param r: 多项式阶数
    :param epsilon: 使用共轭梯度法求解线性方程组Ax=b时要求的精度，默认为1e-5
    :return c: 多项式系数
    """
    if len(x) != len(y):
        return

    # construct A
    A = []
    for i in range(1 + time):
        row = []
        for j in range(1 + time):
            row.append(sum([pow(n, i + j) for n in x]))
        A.append(row)
    A = np.asarray(A, dtype=np.float64)

    # construct b
    b = []
    for i in range(1 + time):
        xi = [pow(n, i) for n in x]
        b.append(np.dot(xi, y))
    b = np.asarray(b, dtype=np.float64)

    # solve Ax=b
    c0 = np.asarray([0] * (r + 1), dtype=np.float64)
    c, _ = conjugate_gradient(A, b, c0, epsilon)
    
    return c
```

### 具体算例与结果

#### 算例

```python
x = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
y = [5.1234, 5.3057, 5.5687, 5.9375, 6.4370, 7.0978, 7.9493, 9.0253, 10.3627]
r = 4
```

#### 结果

```python
c = [5.01031769 0.86743561 2.49929486 2.28321087 1.34649999]
```

## 利用迭代法求解非线性方程及方程组

### 方法简介

一般情况下，非线性方程$f(x)=0$及方程组$f_i(x_1,x_2,\cdots,x_n)=0~(i=1,\cdots,n)$的解不能用解析式表示。因此采用迭代法（也称为逐次逼近法）求其近似解。<br>

1. 求解非线性方程的迭代法有：二分法、简单迭代法、牛顿法、弦割法等。<br>

| 算法名称    | 基本思想                                                     | 说明                                                         | 收敛性                               |
| ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------ |
| 二分法      | 零点存在定理                                                 | 优点：计算简单、方法可靠且总是收敛<br>缺点：收敛速度慢，要求得根比较精确的近似值时需要多次计算函数$f(x)$的值，运算量较大<br>常用于求根的大体范围（即进行根的隔离）或用于求其他快速收敛的迭代法所需的一个初始点 |                                      |
| 简单迭代法  | 将方程$f(x)=0$改写为同解方程$x=\phi(x)$<br>构造迭代格式$x_{k+1}=\phi(x_k)$ | 迭代序列是否收敛以及收敛的快慢同迭代函数$\phi(x)$的选取有关  | 线性收敛                             |
| 牛顿迭代法  | 利用泰勒公式得收敛格式<br>$x_{k+1}=x_k-\frac{f(x_k)}{f'(x_k)}$ | $f(x)$在含根区间$[a,b]$上二阶连续可微且$f'(x)\neq0$，二阶泰勒展开略去二次项 | 二阶收敛                             |
| 改进牛顿法1 | 利用泰勒公式得收敛格式<br>$x_{k+1}=x_k-\frac{2f(x_k)}{(f'(x_k) + sgn(f'(x))\sqrt{f'^2(x_k)-2f(x_k)f''(x_k)})}$ | $f(x)$在含根区间$[a,b]$上三阶连续可微且$f'(x)\neq 0,f''(x)\neq 0$，二阶泰勒展开略去三次项 |                                      |
| 改进牛顿法2 | 对$y=f(x)$的反函数$x=g(y)$泰勒展开得收敛格式<br>$x_{k+1}=x_k-\frac{f(x_k)}{f'(x_k)}-\frac{f''(x_k)}{2f'^3(x_k)}f^2(x_k)$ | $x=g(y)$三阶连续可微，三阶泰勒展开略去三次项                 |                                      |
| 简化牛顿法  | 用$f'(x_0)$或常数$c_0$代替牛顿法中的$f'(x_k)$                | 在实际问题中，若导数$f'(x)$难以计算或计算量较大，可以用此法  |                                      |
| 牛顿下山法  | $x_{k+1}=x_k-\lambda\frac{f(x_k)}{f'(x_k)}$                  | 选取$\lambda$应使$\abs{f(x_{k+1})} < \abs{f(x_k)}$<br>这样做可以放宽牛顿法的初始点$x_0$的选择范围<br>在实际计算时，选取$\lambda$采用试算的办法，即从$\lambda=1$开始，逐次减半直到上式成立为止 |                                      |
| 弦割法      | 用$(x_{k-1}, f(x_{k-1})), (x_k, f(x_k))$或$(x_0, f(x_0)), (x_k, f(x_k))$<br>连线的斜率代替牛顿法中切线的斜率$f'(x_k)$<br>单点弦割法：<br>$x_{k+1}=x_k-\frac{f(x_k)(x_k-x_{k-1})}{f(x_k)-f(x_{k-1})}=\frac{f(x_k)/f(x_{k-1})}{f(x_k)/f(x_{k-1})-1}(x_k-x_{k-1})$<br>两点弦割法：<br>$x_{k+1}=x_k-\frac{f(x_k)(x_k-x_0)}{f(x_k)-f(x_0)}$ | 优点：不计算导数<br>缺点：需高精度计算，因为$f(x_k),f(x_{k-1})$很接近 | 超线性收敛<br>（单点弦割法线性收敛） |
| 改进弦割法  | 对$y=f(x)$的反函数$x=g(y)$作二次牛顿插值得到$N_2(y)$得<br>$x_{k+1}=x_k-\frac{x_k-x_{k-1}}{f(x_k)-f(x_{k-1})}f(x_k)$<br>$+(\frac{x_k-x_{k-1}}{f(x_k)-f(x_{k-1})}-\frac{x_{k-1}-x_{k-2}}{f(x_{k-1})-f(x_{k-2})})\frac{f(x_k)f(x_{k-1})}{f(x_k)-f(x_{k-2})}$ |                                                              |                                      |

2. 求解非线性方程组的迭代法有：简单迭代法、牛顿法、弦割法、布洛依登法等。

   简单迭代法：
   $$
   x^{(k+1)}=\phi(x^{(k)}) \\
   \phi(x)=(\phi_1(x), \phi_2(x), \cdots, \phi_n(x))^T
   $$
   牛顿法：
   $$
   J_f(x^{(k)})(x-x^{(k)})=-f(x^{(k)}) \\
   x^{(k+1)}=x^{(k)}-J_f^{-1}(x^{(k)})f(x^{(k)})
   $$
   其中$J_f$为$f$的雅可比矩阵。
   为了避免对$J_f$求逆，采用下面的方式计算$x^{(k+1)}$
   $$
   \begin{cases}
   J_f(x^{(k)})\Delta x^{(k)} = -f(x^{(k)}) \\
   x^{(k+1)} = x^{(k)} + \Delta x^{(k)}
   \end{cases}
   $$

3. 对于收敛慢的迭代法，我们还可以使用**加速收敛技术**。
   简单迭代法的收敛条件为迭代函数$\phi(x)$满足$\abs{\phi'(x)}\le L<1$（不满足此条件一般不收敛），但是当$L$接近于1时，收敛得相当慢。
   为了使简单迭代法收敛得更快或者将不收敛的迭代格式改造为收敛的迭代格式，将原来的方程$x=\phi(x)$作同解变形，得到
   $$
   x=\psi(x)=\frac{\phi(x)-\omega x}{1-\omega}
   $$
   则迭代格式变为
   $$
   x_{k+1}=\psi(x_k)=\frac{\phi(x_k)-\omega x_k}{1-\omega}
   $$
   为使该迭代格式产生的$\{x_k\}$二阶收敛，应该选取松弛因子$\omega$，使得$\psi'(x^*)=0$，即$\omega=\psi'(x^*)$。因为$x^*$使所求的，未知，所以最佳松弛因子不能准确求出，通常取$\omega=\psi'(\overline{x})$，其中$\overline{x}$是$x^*$的一个好的近似值（如用二分法求得）。

### 伪代码

#### 求解非线性方程

##### 二分法

1. 给出解的存在区间$[a,b],f(a)f(b)<0$，给定允许的误差限和$\varepsilon_1,\varepsilon_2$最大迭代次数$K$；

2. $for~k=0~to~K~do$

   (1) 计算$x_k=\frac{a+b}{2}$；<br>

   (2) 若$\abs{a - b} < \varepsilon_1$或$\abs{f(x_k)} < \varepsilon_2$，则取$x^* \approx x_k$，停止计算；<br>

   (3) 若$k=K$，输出$K$次迭代不满足精度要求的信息，则停机；<br>

   (4) 若$f(a)f(x_k)<0$，则$b=x_k$；否则$a=x_k$；<br>

   $end~do$<br>

##### 简单迭代法

1. 给出$\phi(x)$，在$x^*$附近选取$x_0\in D$，给定允许的误差限$\varepsilon_1,\varepsilon_2$和最大迭代次数$K$；

2. $for~k=0~to~K~do$

   (1) 计算$x_{k+1} = \phi(x_k)$；<br>

   (2) 若$\abs{x_{k+1}-x_k} < \varepsilon_1$或$\abs{f(x_{k+1})} < \varepsilon_2$，则取$x^* \approx x_{k+1}$，停止计算；如果$k=K$，输出$K$次迭代不满足精度要求的信息，则停机；<br>

   $end~do$<br>

##### 牛顿法

1. 在$x^*$附近选取$x_0\in D$，给定允许的误差限$\varepsilon_1,\varepsilon_2$和最大迭代次数$K$；

2. $for~k=0~to~K~do$

   (1) 计算$x_{k+1} = x_k - \frac{f(x_k)}{f'(x_k)}$（简化牛顿法为$x_{k+1} = x_k - \frac{f(x_k)}{f'(x_0)}$或$x_{k+1} = x_k - \frac{f(x_k)}{c_0}$）；<br>

   (2) 若$\abs{x_{k+1} - x_k} < \varepsilon_1$或$\abs{f(x_{k+1})} < \varepsilon_2$，则取$x^* \approx x_{k+1}$，停止计算；如果$k=K$，输出$K$次迭代不满足精度要求的信息，则停机；<br>

   $end~do$<br>

##### 弦割法

1. 在$x^*$附近选取$x_0,x_1\in D$，给定允许的误差限$\varepsilon_1,\varepsilon_2$和最大迭代次数$K$；

2. $for~k=1~to~K~do$

   (1) 计算$x_{k+1} = x_k - \frac{f(x_k)(x_k - x_0)}{f(x_k) - f(x_0)}$；（两点弦割法为$x_{k+1} = x_k - \frac{f(x_k)/f(x_{k-1})}{f(x_k)/f(x_{k-1}) - 1}(x_k - x_{k-1})$）<br>

   (2) 若$\abs{x_{k+1} - x_k} < \varepsilon_1$或$\abs{f(x_{k+1})} < \varepsilon_2$，则取$x^* \approx x_{k+1}$，停止计算；如果$k=K$，输出$K$次迭代不满足精度要求的信息，则停机；<br>

   $end~do$<br>

#### 求解非线性方程组

##### 牛顿法

1. 在$x^*$附近选取$x^{(0)}\in D$，给定允许的误差限$\varepsilon_1,\varepsilon_2$和最大迭代次数$K$；

2. $for~k=0~to~K~do$

   (1) 计算$f(x^{(k)}),J_f(x^{(k)})$；<br>

   (2) 求解关于$\Delta x^{(k)}$的线性方程组$J_f(x^{(k)})\Delta x^{(k)} = -f(x^{(k)})$；<br>

   (3) 令$x^{(k+1)} = x^{(k)} + \Delta x^{(k)}$；<br>

   (4) 若$\parallel \Delta x^{(k)} \parallel / \parallel x^{(k)} \parallel < \varepsilon_1$或$\parallel f(x^{(k+1)} \parallel < \varepsilon_2)$，则取$x^* \approx x^{(k+1)}$，停止计算；如果$k=K$，输出$K$次迭代不满足精度要求的信息，则停机；<br>

   $end~do$<br>

### 代码

#### 求解非线性方程

##### 二分法
```python
def binary_iterative_method(f, x, a, b, epsilon1, epsilon2):
    while True:
        mid = (a + b) / 2  # 求中点
        if abs(f.subs({x: mid})) < epsilon1 or (b - a) < epsilon2:  # 停机条件
            return mid, a, b
        elif f.subs({x: a}) * f.subs({x: mid}) < 0:  # f(a) * f(mid) < 0
            b = mid
        else:  # f(mid) * f(b) < 0
            a = mid
```

##### 简单迭代法

```python
def simple_iterative_method(f, x, x0, epsilon1=1e-8, epsilon2=1e-8):
    xk = x0
    itr = 0
    while True:
        nxk = f.subs({x: xk})  # 迭代格式
        itr += 1
        if abs(f.subs({x: nxk}) - nxk) < epsilon1 or abs(nxk - xk) < epsilon2:  # 停机条件
            return nxk
        xk = nxk
```

##### 牛顿迭代法

```python
def newton_method(f, x, x0, epsilon1=1e-8, epsilon2=1e-8):
    xk = x0
    itr = 0
    while True:
        nxk = xk - f.subs({x: xk}) / f.diff(x).subs({x: xk})  # 迭代格式
        itr += 1
        if abs(f.subs({x: nxk})) < epsilon1 or abs(nxk - xk) < epsilon2:  # 停机条件
            return nxk
        xk = nxk
```

##### 简化牛顿迭代法

```python
def simplified_newton_method(f, x, x0, epsilon1=1e-8, epsilon2=1e-8):
    xk = x0
    c = f.diff(x).subs({x: x0})
    itr = 0
    while True:
        nxk = xk - f.subs({x: xk}) / c  # 迭代格式
        itr += 1
        if abs(f.subs({x: nxk})) < epsilon1 or abs(nxk - xk) < epsilon2:  # 停机条件
            return nxk
        xk = nxk
```

##### 弦割法

```python
def chord_secant_method(f, x, x0, epsilon1=1e-8, epsilon2=1e-8):
    f0 = f.subs({x: x0})
    xk = x0 - f0 / f.diff(x).subs({x: x0})  # 先用牛顿法的迭代格式求x_1
    itr = 1
    while True:
        fk = f.subs({x: xk})
        nxk = xk - fk * (xk - x0) / (fk - f0)  # 迭代格式
        itr += 1
        if abs(f.subs({x: nxk})) < epsilon1 or abs(nxk - xk) < epsilon2:  # 停机条件
            return nxk
        xk = nxk
```

#### 求解非线性方程组

```python
def newton_method(f: list, x: list, x0: list, epsilon1, epsilon2, K: int):
    f = sympy.Matrix(f)
    x = sympy.Matrix(x)
    j = f.jacobian(x)  # 雅可比矩阵
    xk = sympy.Matrix(x0)
    k = 0
    while k < K:
        fk = f.subs({x[i]: xk[i] for i in range(len(x))})
        jk = j.subs({x[i]: xk[i] for i in range(len(x))})
        delta = jk.solve(-fk)  # 求delta_k
        nxk = xk + delta  # 计算x_(k+1) = x_k + delta_k
        nfk = f.subs({x[i]: nxk[i] for i in range(len(x))})
        if delta.norm() < epsilon1 * xk.norm() or nfk.norm() < epsilon2:  # 停机条件
            return nxk
        xk = nxk
        k += 1
    print(f'cannot achieve given precision in {K} iterations')
```

### 具体算例与结果

#### 求解非线性方程

##### 算例

$$
f(x) = x^6 - 5x^5 + 3x^4 + x^3 -7x^2 + 7x - 20 = 0
$$

找出$f(x)$在$[-1,5]$上的全部实根。

##### 结果

|                  | 迭代格式（二分法得到解的存在区间为$[4.296875, 4.34375]$）    | 迭代次数 | 结果（$\varepsilon = 10^{-8}$） |
| ---------------- | ------------------------------------------------------------ | -------- | ------------------------------- |
| 简单迭代法       | $x_{k+1}=\phi(x_k)=(5x_k^5-3x_k^4-x_k^3+7x_k^2-7x_k+20)^{\frac{1}{6}}$ | 78       | 4.33375538245763                |
| 加速的简单迭代法 | $\overline{x}=4.3203125,\omega=\phi'(\overline{x})=0.855074089373259$<br>$x_{k+1}=\psi(x_k)=\frac{\phi(x_k)-\omega x_k}{1-\omega}$ | 3        | 4.33375544707089                |
| 牛顿法           | $x_{k+1}=x_k-\frac{x_k^6-5x_k^5+3x_k^4+x_k^3-7x_k^2+7x_k-20}{6x_k^5-25x_k^4+12x_k^3+3x_k^2-14x_k+7}$ | 3        | 4.33375544692000                |
| 简化牛顿法       | $x_0=4.3203125,f'(x_0)=1291.34801688202$<br>$x_{k+1}=x_k-\frac{x_k^6-5x_k^5+3x_k^4+x_k^3-7x_k^2+7x_k-20}{1291.34801688202}$ | 5        | 4.33375544715679                |
| 弦割法           | $x_0=4.3203125,x_1=4.33397174648243,f(x_0)=-17.6388408571838$<br>$x_{k+1}=x_k-\frac{(x_k^6-5x_k^5+3x_k^4+x_k^3-7x_k^2+7x_k-20)(x_k-4.3203125)}{x_k^6-5x_k^5+3x_k^4+x_k^3-7x_k^2+7x_k-2.36115914281618}$ | 5        | 4.33375544693395                |

#### 求解非线性方程组

##### 算例

$$
\begin{cases}
x_1^2 + x_2^2 + x_3^2 - 1.0 = 0, \\
2x_1^2 + x_2^2 - 4x_3 = 0, \\
3x_1^2 - 4x_2^2 + x_3^2 = 0, \\
\end{cases}
$$

初始向量$x^{(0)}=(1.0,1.0,1.0)^T$

##### 结果

$x^*=(0.69828861,0.6285243,0.34256419)^T$

## 各工程领域实际问题的计算求解

### 问题介绍

2021年上映的《长津湖》是一部制作精良的军事大片，以抗美援朝战争第二次战役中的长津湖战役为背景，
讲述了一段波澜壮阔的历史，在极寒严酷环境下，中国人民志愿军东线作战部队凭着钢铁意志和英勇无畏的战斗精神，
扭转战场态势，为长津湖战役胜利做出重要贡献的故事。

截至2021年11月底，经过增加排片，共约60天的《长津湖》以57.3亿元的票房，
超过《战狼2》成为了中国电影票房榜的新冠军。现在该片又延期到12月30日。

现需根据其10月与11月的票房数据，预测其12月的票房。

### 解决方案

通过对《长津湖》10月与11月的票房数据进行初步分析，我们发现，其票房基本以7天为周期进行波动。

<img src="E:\计算方法\上机\cmhomework\figures\chang_jin_hu_daily_box.jpg" style="zoom:67%;" />

这符合常识：周一到周五为工作日，票房较低，周六、周日为休息日，票房较高。

因此，我们剔除了9.29-10.10期间的数据（处于国庆假期，不符合票房波动规律），从10.11日（国庆假期后的第一个周一）开始，将其票房数据按周划分，得到$x=(1, 2, \cdots, 7)$和$y = (y_1, y_2, \cdots, y_7)$，同时，由于票房不可能为负数，我们在得到的数据点序列中加入$(12, \varepsilon),\varepsilon\in[0,y_7]$表示排片结束后票房为零。

处理后得到的数据：
$$
x = [1, 2, 3, 4, 5, 6, 7, 12] \\
y = [70548.72330000001, 32690.966300000004, 17925.3879, 8546.800000000001, 4708.8, 3761.5, 3729.4, 0]
$$
<img src="E:\计算方法\上机\cmhomework\figures\chang_jin_hu_weekly_box.jpg" style="zoom:67%;" />

接着我们采用最小二乘拟合的方式，取$\phi_0(x)=1,\phi_1(x)=x^{-1},\phi_2(x)=x$拟合其票房曲线。

### 代码

```python
import csv
import numpy as np
import matplotlib.pyplot as plt


def least_square_fitting(x: [], y: [], phi: []):
    if len(x) != len(y):
        return

    # construct A
    A = []
    for i in range(len(phi)):
        row = []
        for j in range(len(phi)):
            row.append(sum([phi[i](n) * phi[j](n) for n in x]))
        A.append(row)
    A = np.asarray(A, dtype=np.float64)

    # construct b
    b = []
    for i in range(len(phi)):
        xi = [phi[i](n) for n in x]
        b.append(np.dot(xi, y))
    b = np.asarray(b, dtype=np.float64)

    c = np.linalg.solve(A, b)
    return c


def predict(data: str, bound):
    """
    :param data: path to .csv data file
    :return: factors of the fitting polynomial
    """
    with open(data, encoding='utf-8') as f:
        reader = csv.reader(f)

        # 剔除10-11日之前的数据
        for _ in range(13):
            next(reader)

        daily_box = [float(row[1]) for row in reader]

        n_day = len(daily_box)
        bias = n_day % 7
        weekly_box = [sum(daily_box[i: i + 7]) for i in range(bias, n_day, 7)]
        week = [i + 1 for i in range(len(weekly_box))]

        # 增加数据点(12, 0)
        week.append(12)
        weekly_box.append(bound)

        phi = [
            lambda x: 1,
            lambda x: 1 / x,
            lambda x: x
        ]

        c = least_square_fitting(week, weekly_box, phi)

        predictor = lambda x: sum(c[i] * phi[i](x) for i in range(len(c)))
        dec = []
        for i in range(8, 12):
            dec.append(predictor(i))
        print(dec)
        print(sum(dec))

        x = np.linspace(1, week[-1], 1000)
        y = [sum(c[i] * phi[i](w) for i in range(len(c))) for w in x]

        plt.plot(week, weekly_box)
        plt.plot(x, y)
        plt.savefig('figures/chang_jin_hu_fitting' + str(bound) + '.jpg')
        plt.show()

        print(c)


if __name__ == "__main__":
    for i in range(0, 1100, 500):
        predict('E:/计算方法/上机/长津湖每日票房.csv', i)
```

### 结果

添加的数据点$(12, \varepsilon)$中$\varepsilon$的值不同，得到的结果不同，下面列举了$\varepsilon=0,500,1000$时的拟合结果

| $\varepsilon$ | $(c_0,c_1,c_2)$                                              | $(y_8,y_9,y_{10},y_{11})$                                    | $total$              |
| ------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | -------------------- |
| 0             | $c_0=-12024.49753333$<br>$c_1=83035.62342399$<br>$c_2=397.21032937$ | $y_8=1532.6380296348439$<br>$y_9=776.5758114507312$<br>$y_{10}=251.16810277770855$<br>$y_{11}=-106.49087170543498$ | $2453.8910721578486$ |
| 500           | $c_0=-12401.9442443$<br/>$c_1=83389.329595$<br/>$c_2=461.53504441$ | $y_8=1714.0023103417607$<br/>$y_9=1017.3522214856785$<br/>$y_{10}=552.3391592823891$<br/>$y_{11}=255.78938919021766$ | $3539.483080300046$  |
| 1000          | $c_0=-12779.39095526$<br/>$c_1=83743.03576602$<br/>$c_2=525.85975944$ | $y_8=1895.3665910486816$<br/>$y_9=1258.128631520627$<br/>$y_{10}=853.5102157870688$<br/>$y_{11}=618.0696500858658$ | $4625.075088442243$  |

<img src="E:\计算方法\上机\cmhomework\figures\chang_jin_hu_fitting0.jpg" style="zoom: 67%;" />

<img src="E:\计算方法\上机\cmhomework\figures\chang_jin_hu_fitting500.jpg" style="zoom:67%;" />

<img src="E:\计算方法\上机\cmhomework\figures\chang_jin_hu_fitting1000.jpg" style="zoom:67%;" />

## 程序使用说明

### 环境

需要安装numpy, sympy

### 使用

#### 运行程序内置测试用例：

```
python main.py -test
```

#### 以文件（json格式）方式提供输入并计算：

```
python main.py -input <path to input file>
```

#### input文件格式示例（位于inputs/）

##### 共轭梯度法：

```json
{
  "type": "conjugate_gradient",
  "A": [
    [2, 0, 1],
    [0, 1, 0],
    [1, 0, 2]
  ],
  "b": [3, 1, 3],
  "x0": [0, 0, 0],
  "epsilon": 1e-5
}
```

epsilon可选，默认为1e-8。

##### 最小二乘拟合：

```json
{
  "type": "ls_fitting",
  "x": [0, 0.25, 0.50, 0.75, 1.00],
  "y": [1.0000, 1.2840, 1.6487, 2.1170, 2.7183],
  "order": 2,
  "epsilon": 1e-5
}
```

order为多项式系数。

epsilon可选，默认为1e-8。

##### 迭代法求解非线性方程：

```json
{
  "type": "iterative_method_equation",
  "x": "x",
  "f": "x**6 - 5*x**5 + 3*x**4 + x**3 - 7*x**2 + 7*x - 20",
  "range": [-1, 5],
  "epsilon1": 1e-5,
  "epsilon2": 1e-5
}
```

可以指定解的存在区间range，也可以指定初始值x0。

epsilon1, epsilon2可选，默认为1e-8。

##### 迭代法求解非线性方程组：

```json
{
  "type": "iterative_method_equations",
  "x": [
    "x1",
    "x2"
  ],
  "f": [
    "4*x1 - x2 + 0.1*exp(x1) - 1",
    "-x1 + 4*x2 + x1**2 / 8"
  ],
  "x0": [0, 0],
  "epsilon1": 1e-8,
  "epsilon2": 1e-8,
  "K": 10
}
```

epsilon1, epsilon2可选，默认为1e-8。

K可选，默认为100