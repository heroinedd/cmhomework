import sympy


def newton_method(f: list, x: list, x0: list, epsilon1=1e-5, epsilon2=1e-5, K=100):
    f = sympy.Matrix(f)
    x = sympy.Matrix(x)
    j = f.jacobian(x)
    xk = sympy.Matrix(x0)
    k = 0
    while k < K:
        fk = f.subs({x[i]: xk[i] for i in range(len(x))})
        jk = j.subs({x[i]: xk[i] for i in range(len(x))})
        delta = jk.solve(-fk)
        nxk = xk + delta
        nfk = f.subs({x[i]: nxk[i] for i in range(len(x))})
        if delta.norm() < epsilon1 * xk.norm() or nfk.norm() < epsilon2:
            return nxk.evalf()
        xk = nxk
        k += 1
    print(f'cannot achieve given precision in {K} iterations')


def example1():
    example = """
    f(x) = { x_1^2 + x_2^2 +x_3^2 - 1.0 = 0
             2x_1^2 + x_2^2 - 4x_3^2 = 0
             3x_1^2 - 4x_2^2 + x_3^2 = 0 }
    x^(0) = (1.0, 1.0, 1.0)^T
    """
    print('迭代法求非线性方程组示例一：')
    print(example)
    x1, x2, x3 = sympy.symbols('x1, x2, x3')
    f1 = (x1**2) + (x2**2) + (x3**2) - 1
    f2 = (2 * x1**2) + (x2**2) - (4 * x3)
    f3 = (3 * x1**2) - (4 * x2**2) + (x3**2)
    f = [f1, f2, f3]
    x = [x1, x2, x3]
    x0 = [1, 1, 1]
    epsilon = 1e-8
    ans = newton_method(f, x, x0, epsilon, epsilon, 30)
    print('结果：')
    print(ans)


def example2():
    example = """
    f(x) = { 4x_1 - x_2 + 0.1exp(x_1) - 1 = 0
             -x_1 + 4x_2 + x_1^2 / 8 = 0 }
    x^(0) = (0, 0)^T
    """
    print('迭代法求非线性方程组示例二：')
    print(example)
    x1, x2 = sympy.symbols('x1, x2')
    f1 = (4 * x1) - x2 + (0.1 * sympy.exp(x1)) - 1
    f2 = -x1 + (4 * x2) + (x1 ** 2 / 8)
    f = [f1, f2]
    x = [x1, x2]
    x0 = [0, 0]
    epsilon = 1e-8
    ans = newton_method(f, x, x0, epsilon, epsilon, 10)
    print('结果：')
    print(ans)


if __name__ == "__main__":
    example1()
    example2()
