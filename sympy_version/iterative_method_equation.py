import sympy


def divide_range(f, x, a, b, length=0.05):
    if b - a < length:
        return [[a, b]] if f.subs({x: a}) * f.subs({x: b}) < 0 else []
    mid = (a + b) / 2
    ret = divide_range(f, x, a, mid, length)
    ret.extend(divide_range(f, x, mid, b, length))
    return ret


def binary_iterative_method(f, x, a, b, epsilon1=1e-8, epsilon2=1e-8):
    while True:
        mid = (a + b) / 2
        if abs(f.subs({x: mid})) < epsilon1 or (b - a) < epsilon2:
            return mid, a, b
        elif f.subs({x: a}) * f.subs({x: mid}) < 0:
            b = mid
        else:
            a = mid


def simple_iterative_method(f, x, x0, epsilon1=1e-8, epsilon2=1e-8):
    xk = x0
    itr = 0
    while True:
        nxk = f.subs({x: xk})
        itr += 1
        # print(f'x{itr}={nxk}')
        if abs(f.subs({x: nxk}) - nxk) < epsilon1 or abs(nxk - xk) < epsilon2:
            return nxk
        xk = nxk


def newton_method(f, x, x0, epsilon1=1e-8, epsilon2=1e-8):
    xk = x0
    itr = 0
    while True:
        nxk = xk - f.subs({x: xk}) / f.diff(x).subs({x: xk})
        itr += 1
        # print(f'x{itr}={nxk}')
        if abs(f.subs({x: nxk})) < epsilon1 or abs(nxk - xk) < epsilon2:
            return nxk
        xk = nxk


def simplified_newton_method(f, x, x0, epsilon1=1e-8, epsilon2=1e-8):
    xk = x0
    c = f.diff(x).subs({x: x0})
    itr = 0
    while True:
        nxk = xk - f.subs({x: xk}) / c
        itr += 1
        # print(f'x{itr}={nxk}')
        if abs(f.subs({x: nxk})) < epsilon1 or abs(nxk - xk) < epsilon2:
            return nxk
        xk = nxk


def chord_secant_method(f, x, x0, epsilon1=1e-8, epsilon2=1e-8):
    f0 = f.subs({x: x0})
    xk = x0 - f0 / f.diff(x).subs({x: x0})
    itr = 1
    while True:
        fk = f.subs({x: xk})
        nxk = xk - fk * (xk - x0) / (fk - f0)
        itr += 1
        # print(f'x{itr}={nxk}')
        if abs(f.subs({x: nxk})) < epsilon1 or abs(nxk - xk) < epsilon2:
            return nxk
        xk = nxk


def example():
    eg = """
    f(x) = x^6 - 5x^5 + 3x^4 + x^3 - 7x^2 + 7x - 20 = 0
    find all solutions in [-1, 5]
    """
    print('迭代法求解非线性方程示例：')
    print(eg)

    epsilon1 = 1e-8
    epsilon2 = 1e-8

    x = sympy.symbols('x')
    f = x ** 6 - 5 * x ** 5 + 3 * x ** 4 + x ** 3 - 7 * x ** 2 + 7 * x - 20
    phi = sympy.Pow(5 * x ** 5 - 3 * x ** 4 - x ** 3 + 7 * x ** 2 - 7 * x + 20, 1 / 6)
    omega = (25 * x ** 4 - 12 * x ** 3 - 3 * x ** 2 + 14 * x - 7) * \
            sympy.Pow(5 * x ** 5 - 3 * x ** 4 - x ** 3 + 7 * x ** 2 - 7 * x + 20, -5 / 6) / 6
    ranges = divide_range(f, x, -1, 5)
    for range in ranges:
        # print(range)
        x0 = (range[0] + range[1]) / 2

        # simple iterative method
        # print('Simple Iterative Method')
        print('简单迭代法')
        ans = simple_iterative_method(phi, x, x0, epsilon1, epsilon2)
        print(ans)

        # accelerated simple iterative method
        # print('Accelerated Simple Iterative Method')
        print('加速简单迭代法')
        tmp = omega.subs({x: x0})
        psi = (phi - tmp * x) / (1 - tmp)
        ans = simple_iterative_method(psi, x, x0, epsilon1, epsilon2)
        print(ans)

        # newton method
        # print('Newton Method')
        print('牛顿法')
        ans = newton_method(f, x, x0, epsilon1, epsilon2)
        print(ans)

        # simplified newton method
        # print('Simplified Newton Method')
        print('简化牛顿法')
        ans = simplified_newton_method(f, x, x0, epsilon1, epsilon2)
        print(ans)

        # chord secant method
        # print('Chord Secant Method')
        print('单点弦割法')
        ans = chord_secant_method(f, x, x0, epsilon1, epsilon2)
        print(ans)


if __name__ == "__main__":
    example()
