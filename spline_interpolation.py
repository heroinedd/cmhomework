from numpy import linalg


def difference_quotient(x: list, y: list):
    if len(y) == 1:
        return y[0]
    else:
        return (difference_quotient(x[1:], y[1:]) - difference_quotient(x[:-1], y[:-1])) / (x[-1] - x[0])


def spline_interpolation(x: list, y: list):
    n = len(y) - 1
    h = [x[i + 1] - x[i] for i in range(len(x) - 1)]
    mu = [h[i] / (h[i] + h[i + 1]) for i in range(len(h) - 1)]
    lamda = [1 - mu[i] for i in range(len(mu))]
    d = [6 * difference_quotient(x[i: i + 3], y[i: i + 3]) for i in range(len(x) - 2)]
    # construct three moment equation
    A = []
    for i in range(len(mu)):
        row = []
        row.extend([0] * max(0, i - 1))
        if i == 0:
            row.extend([2, lamda[i]])
        elif i == n - 2:
            row.extend([mu[i], 2])
        else:
            row.extend([mu[i], 2, lamda[i]])
        row.extend([0] * max(0, n - 1 - len(row)))
        A.append(row)
    M = linalg.solve(A, d)
    M_ret = [0]
    M_ret.extend(M.tolist())
    M_ret.append(0)
    return M_ret


def numerical_integration(x: list, y: list):
    n = len(y) - 1
    h = [x[i + 1] - x[i] for i in range(n)]
    M = spline_interpolation(x, y)
    ans = sum([h[i] * (y[i - 1] + y[i]) for i in range(1, n)]) / 2 \
          - sum([(M[i - 1] + M[i]) * (h[i]**3) for i in range(1, n)]) / 24
    return ans


def example1():
    x = [-3, -1, 0, 3, 4]
    y = [7, 11, 26, 56, 29]
    numerical_integration(x, y)


if __name__ == "__main__":
    example1()
