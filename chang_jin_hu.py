import csv
import numpy as np
import matplotlib.pyplot as plt


def least_square_fitting(x: [], y: [], phi: []):
    if len(x) != len(y):
        return

    # construct A
    A = []
    for i in range(len(phi)):
        row = []
        for j in range(len(phi)):
            row.append(sum([phi[i](n) * phi[j](n) for n in x]))
        A.append(row)
    A = np.asarray(A, dtype=np.float64)

    # construct b
    b = []
    for i in range(len(phi)):
        xi = [phi[i](n) for n in x]
        b.append(np.dot(xi, y))
    b = np.asarray(b, dtype=np.float64)

    c = np.linalg.solve(A, b)
    return c


def predict(data: str, bound):
    """
    :param data: path to .csv data file
    :return: factors of the fitting polynomial
    """
    with open(data, encoding='utf-8') as f:
        reader = csv.reader(f)

        # 剔除10-11日之前的数据
        for _ in range(13):
            next(reader)

        daily_box = [float(row[1]) for row in reader]

        n_day = len(daily_box)
        bias = n_day % 7
        weekly_box = [sum(daily_box[i: i + 7]) for i in range(bias, n_day, 7)]
        week = [i + 1 for i in range(len(weekly_box))]

        # 增加数据点(12, 0)
        week.append(12)
        weekly_box.append(bound)

        phi = [
            lambda x: 1,
            lambda x: 1 / x,
            lambda x: x
        ]

        c = least_square_fitting(week, weekly_box, phi)

        predictor = lambda x: sum(c[i] * phi[i](x) for i in range(len(c)))
        dec = []
        for i in range(8, 12):
            dec.append(predictor(i))
        print(dec)
        print(sum(dec))

        x = np.linspace(1, week[-1], 1000)
        y = [sum(c[i] * phi[i](w) for i in range(len(c))) for w in x]

        plt.plot(week, weekly_box)
        plt.plot(x, y)
        plt.savefig('figures/chang_jin_hu_fitting' + str(bound) + '.jpg')
        plt.show()

        print(c)


if __name__ == "__main__":
    for i in range(0, 1100, 500):
        predict('inputs/长津湖每日票房.csv', i)
