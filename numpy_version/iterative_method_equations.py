import numpy as np
from numpy import linalg, exp


def newton_method(f: list, j: list, x0: np.ndarray, epsilon1: np.float64, epsilon2: np.float64, K: int):
    xk = x0
    k = 0
    while k < K:
        fk = np.asarray([func(xk) for func in f], dtype=np.float64)
        jk = np.asarray([[func(xk) for func in row] for row in j], dtype=np.float64)
        delta = linalg.solve(jk, -fk)
        nxk = xk + delta
        nfk = np.asarray([func(nxk) for func in f], dtype=np.float64)
        if linalg.norm(delta) < epsilon1 * linalg.norm(xk) or linalg.norm(nfk) < epsilon2:
            return nxk
        xk = nxk
        k += 1
    print(f'cannot achieve given precision in {K} iterations')


def example1():
    example = """
    f(x) = { x_1^2 + x_2^2 +x_3^2 - 1.0 = 0
             2x_1^2 + x_2^2 - 4x_3^2 = 0
             3x_1^2 - 4x_2^2 + x_3^2 = 0 }
    x^(0) = (1.0, 1.0, 1.0)^T
    """
    print('迭代法求非线性方程组示例一：')
    print(example)
    f = [
        lambda x: (x[0]**2) + (x[1]**2) + (x[2]**2) - 1,
        lambda x: (2 * x[0]**2) + (x[1]**2) - (4 * x[2]),
        lambda x: (3 * x[0]**2) - (4 * x[1]**2) + (x[2]**2)
    ]
    j = [
        [lambda x: 2 * x[0],
         lambda x: 2 * x[1],
         lambda x: 2 * x[2]],
        [lambda x: 4 * x[0],
         lambda x: 2 * x[1],
         lambda x: -4],
        [lambda x: 6 * x[0],
         lambda x: -8 * x[1],
         lambda x: 2 * x[2]]
    ]
    x0 = np.asarray([1, 1, 1], dtype=np.float64)
    epsilon = np.float64(1e-8)
    x = newton_method(f, j, x0, epsilon, epsilon, 30)
    print('结果：')
    print(x)


def example2():
    example = """
    f(x) = { 4x_1 - x_2 + 0.1exp(x_1) - 1 = 0
             -x_1 + 4x_2 + x_1^2 / 8 = 0 }
    x^(0) = (0, 0)^T
    """
    print('迭代法求非线性方程组示例二：')
    print(example)
    f = [
        lambda x: (4 * x[0]) - x[1] + (0.1 * exp(x[0])) - 1,
        lambda x: -x[0] + (4 * x[1]) + (x[0]**2 / 8)
    ]
    j = [
        [lambda x: 4 + (0.1 * exp(x[0])),
         lambda x: -1],
        [lambda x: -1 + x[0] / 4,
         lambda x: 4]
    ]
    x0 = np.asarray([0, 0], dtype=np.float64)
    epsilon = np.float64(1e-8)
    x = newton_method(f, j, x0, epsilon, epsilon, 10)
    print('结果：')
    print(x)


if __name__ == "__main__":
    example1()
    example2()
