def dx(func, x):
    f0 = func(x)
    f1 = func(x + 1e-10)
    return (f1 - f0) * 1e10


def divide_range(func, a, b, length=0.05):
    if b - a < length:
        return [[a, b]] if func(a) * func(b) < 0 else []
    mid = (a + b) / 2
    ret = divide_range(func, a, mid, length)
    ret.extend(divide_range(func, mid, b, length))
    return ret


def binary_iterative_method(func, a, b, epsilon1, epsilon2):
    while True:
        x = (a + b) / 2
        if abs(func(x)) < epsilon1 or (b - a) < epsilon2:
            return x, a, b
        elif func(a) * func(x) < 0:
            b = x
        else:
            a = x


def simple_iterative_method(func, x0, epsilon1=1e-8, epsilon2=1e-8):
    x = x0
    itr = 0
    while True:
        nx = func(x)
        itr += 1
        # print(f'x{itr}={nx}')
        if abs(func(nx) - nx) < epsilon1 or abs(nx - x) < epsilon2:
            return nx
        x = nx


def newton_method(func, x0, epsilon1=1e-8, epsilon2=1e-8):
    x = x0
    itr = 0
    while True:
        nx = x - func(x) / dx(func, x)
        itr += 1
        # print(f'x{itr}={nx}')
        if abs(func(nx)) < epsilon1 or abs(nx - x) < epsilon2:
            return nx
        x = nx


def simplified_newton_method(func, x0, epsilon1=1e-8, epsilon2=1e-8):
    x = x0
    c = dx(func, x0)
    itr = 0
    while True:
        nx = x - func(x) / c
        itr += 1
        # print(f'x{itr}={nx}')
        if abs(func(nx)) < epsilon1 or abs(nx - x) < epsilon2:
            return nx
        x = nx


def chord_secant_method(func, x0, epsilon1=1e-8, epsilon2=1e-8):
    f0 = func(x0)
    x = x0 - f0 / dx(func, x0)
    itr = 1
    while True:
        f = func(x)
        nx = x - f * (x - x0) / (f - f0)
        itr += 1
        # print(f'x{itr}={nx}')
        if abs(func(nx)) < epsilon1 or abs(nx - x) < epsilon2:
            return nx
        x = nx


def example():
    eg = """
    f(x) = x^6 - 5x^5 + 3x^4 + x^3 - 7x^2 + 7x - 20 = 0
    find all solutions in [-1, 5]
    """
    print('迭代法求解非线性方程示例：')
    print(eg)

    epsilon1 = 1e-8
    epsilon2 = 1e-8

    func = lambda x: x ** 6 - 5 * x ** 5 + 3 * x ** 4 + x ** 3 - 7 * x ** 2 + 7 * x - 20
    fai = lambda x: pow(5 * x ** 5 - 3 * x ** 4 - x ** 3 + 7 * x ** 2 - 7 * x + 20, 1 / 6)
    omega = lambda x: (25 * x ** 4 - 12 * x ** 3 - 3 * x ** 2 + 14 * x - 7) * \
                      pow(5 * x ** 5 - 3 * x ** 4 - x ** 3 + 7 * x ** 2 - 7 * x + 20, -5 / 6) / 6
    ranges = divide_range(func, -1, 5)
    print(ranges)
    for range in ranges:
        print(range)
        x0 = (range[0] + range[1]) / 2

        # simple iterative method
        # print('Simple Iterative Method')
        print('简单迭代法')
        ans = simple_iterative_method(fai, x0, epsilon1, epsilon2)
        print(ans)

        # accelerated simple iterative method
        # print('Accelerated Simple Iterative Method')
        print('加速简单迭代法')
        tmp = omega(x0)
        psi = lambda x: (fai(x) - tmp * x) / (1 - tmp)
        ans = simple_iterative_method(psi, x0, epsilon1, epsilon2)
        print(ans)

        # newton method
        # print('Newton Method')
        print('牛顿法')
        ans = newton_method(func, x0, epsilon1, epsilon2)
        print(ans)

        # newton method
        # print('Newton Method')
        print('牛顿法')
        ans = simplified_newton_method(func, x0, epsilon1, epsilon2)
        print(ans)

        # chord secant method
        # print('Chord Secant Method')
        print('单点弦割法')
        ans = chord_secant_method(func, x0, epsilon1, epsilon2)
        print(ans)


if __name__ == "__main__":
    example()
