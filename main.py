import argparse
import json
import sympy

import conjugate_gradient
import ls_fitting
from sympy_version import iterative_method_equation, iterative_method_equations

import numpy as np


def parse_conjugate_gradient(j):
    A = np.asarray(j['A'], dtype=np.float64)
    b = np.asarray(j['b'], dtype=np.float64)
    x0 = np.asarray(j['x0'], dtype=np.float64)
    epsilon = np.float64(j['epsilon']) if 'epsilon' in j else np.float64(1e-8)
    ans, _ = conjugate_gradient.conjugate_gradient(A, b, x0, epsilon)
    print(ans)


def parse_ls_fitting(j):
    x = j['x']
    y = j['y']
    order = j['order']
    epsilon = j['epsilon'] if 'epsilon' in j else 1e-8
    ans = ls_fitting.least_square_fitting(x, y, order, epsilon)
    print(ans)


def parse_iterative_method_equation(j):
    x = j['x']
    f = j['f']
    x = sympy.symbols(x)
    f = sympy.simplify(f)
    epsilon1 = j['epsilon1'] if 'epsilon1' in j else 1e-8
    epsilon2 = j['epsilon2'] if 'epsilon2' in j else 1e-8
    if 'range' in j:
        ranges = iterative_method_equation.divide_range(f, x, j['range'][0], j['range'][1])
        ans = []
        for r in ranges:
            ans.append(iterative_method_equation.newton_method(f, x, sum(r) / 2, epsilon1, epsilon2))
        print(ans)
    elif 'x0' in j:
        ans = iterative_method_equation.newton_method(f, x, j['x0'], epsilon1, epsilon2)
        print(ans)


def parse_iterative_method_equations(j):
    vars = j['x']
    funcs = j['f']
    x = [sympy.symbols(var) for var in vars]
    f = [sympy.simplify(func) for func in funcs]
    print(f)
    x0 = j['x0']
    epsilon1 = j['epsilon1'] if 'epsilon1' in j else 1e-8
    epsilon2 = j['epsilon2'] if 'epsilon2' in j else 1e-8
    K = j['K'] if 'K' in j else 100
    ans = iterative_method_equations.newton_method(f, x, x0, epsilon1, epsilon2, K)
    print(ans)


def parse_input(input: str):
    with open(input, 'r', encoding='utf-8') as f:
        j = json.load(f)
        typ = j['type'].lower()
        if typ == 'conjugate_gradient':
            parse_conjugate_gradient(j)
        elif typ == 'ls_fitting':
            parse_ls_fitting(j)
        elif typ == 'iterative_method_equation':
            parse_iterative_method_equation(j)
        elif typ == 'iterative_method_equations':
            parse_iterative_method_equations(j)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-test', action="store_true", help='run examples')
    parser.add_argument("-input", default=None, help='file name of input file (.json format)', type=str)
    args = parser.parse_args()

    if args.test:
        print("----------------------------------------------------------------")
        conjugate_gradient.example()
        print("----------------------------------------------------------------")
        ls_fitting.example1()
        ls_fitting.example2()
        print("----------------------------------------------------------------")
        iterative_method_equation.example()
        print("----------------------------------------------------------------")
        iterative_method_equations.example1()
        iterative_method_equations.example2()
        print("----------------------------------------------------------------")
    elif args.input is not None:
        parse_input(args.input)
