from conjugate_gradient import conjugate_gradient
import numpy as np


def least_square_fitting(x: [], y: [], order: int, epsilon: np.float64):
    if len(x) != len(y):
        return

    # construct A
    A = []
    for i in range(1 + order):
        row = []
        for j in range(1 + order):
            row.append(sum([pow(n, i + j) for n in x]))
        A.append(row)
    A = np.asarray(A, dtype=np.float64)

    # construct b
    b = []
    for i in range(1 + order):
        xi = [pow(n, i) for n in x]
        b.append(np.dot(xi, y))
    b = np.asarray(b, dtype=np.float64)

    # construct c0
    c0 = np.asarray([0] * (order + 1), dtype=np.float64)

    c, _ = conjugate_gradient(A, b, c0, epsilon)
    return c


def example1():
    eg = """
    x = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
    y = [5.1234, 5.3057, 5.5687, 5.9375, 6.4370, 7.0978, 7.9493, 9.0253, 10.3627]
    """
    print('最小二乘拟合示例一：')
    print(eg)
    x = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
    y = [5.1234, 5.3057, 5.5687, 5.9375, 6.4370, 7.0978, 7.9493, 9.0253, 10.3627]
    epsilon = np.float64(1e-5)
    c = least_square_fitting(x, y, 4, epsilon)
    print('结果：')
    print(c)


def example2():
    eg = """
    x = [0, 0.25, 0.50, 0.75, 1.00]
    y = [1.0000, 1.2840, 1.6487, 2.1170, 2.7183]
    """
    print('最小二乘拟合示例一：')
    print(eg)
    x = [0, 0.25, 0.50, 0.75, 1.00]
    y = [1.0000, 1.2840, 1.6487, 2.1170, 2.7183]
    epsilon = np.float64(1e-5)
    c = least_square_fitting(x, y, 2, epsilon)
    print('结果：')
    print(c)


if __name__ == "__main__":
    example1()
    example2()
